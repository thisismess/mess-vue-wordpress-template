<?php


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


$protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';

define('WP_SITEURL', $protocol . '://' . $_SERVER['HTTP_HOST']);
define('WP_HOME',    $protocol . '://' . $_SERVER['HTTP_HOST']);
define('WP_PATH', basename(realpath(__DIR__ . '/..')));

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

if (getenv('DEBUG')) {
    define('DB_NAME', 'PROJECT-NAME');
    /** MySQL database username */
    define('DB_USER', 'root');
    /** MySQL database password */
    define('DB_PASSWORD', '');
    /** MySQL hostname */
    define('DB_HOST', '127.0.0.1');
    /** Database Charset to use in creating database tables. */
    define('DB_CHARSET', 'utf8mb4');
    /** The Database Collate type. Don't change this if in doubt. */
    define('DB_COLLATE', '');   
} else {
    define('DB_NAME', 'PROJECT-NAME');
    /** MySQL database username */
    define('DB_USER', 'root');
    /** MySQL database password */
    define('DB_PASSWORD', '');
    /** MySQL hostname */
    define('DB_HOST', 'localhost');
    /** Database Charset to use in creating database tables. */
    define('DB_CHARSET', 'utf8mb4');
    /** The Database Collate type. Don't change this if in doubt. */
    define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'T7VA!`(XRX-8a0mshJ3)16:SY+9jq-M!7|oll(;[;76A{Z*eg=04=O4ojNt |&3E');
define('SECURE_AUTH_KEY',  'zYl`j7%bAY?X_L ^)8I=lYz74KlZ!w#4m@5zLf.*y/1$MZZMO9c38-HL5NfVn+9|');
define('LOGGED_IN_KEY',    ',Mk]D($|inNk- .|rgHuS`lQ<JMkvaxT6-Hm0;R,%RcHvD+u.r-bk]l(:,yb]oaU');
define('NONCE_KEY',        'a0.lTFFQ9gzfn_gu%k,YShg+a!bAG@+H-Br17>O/$Z|Qa||oLpXj+4YQg|L+_lth');
define('AUTH_SALT',        's)/?73OxkQ|(+i5lAXRN+JF_xYONOKzGh@NSvyx%Kf9Gfm!K*{U,uIf$<vcSciho');
define('SECURE_AUTH_SALT', '%;M-(Hm[Icb_G1#|Vs`C/!-6-B`wqiuC<C@K#9+k2vp2)D9wlXxZWjC0>qc^A-gC');
define('LOGGED_IN_SALT',   '1u+I6-|Dd+RHb?zQmte%2L.+{PRwcNl!fo,%w/nVJn8S7ZkkCg~z@|D}8LE9+B&;');
define('NONCE_SALT',       'z92{Oe@.E-1;D<eAjs>+Y}Q(Fq$|Wn|5AzJq/Dc)&4^go@V;/-TnIFn|RJ8a$Dz@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'aa_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
