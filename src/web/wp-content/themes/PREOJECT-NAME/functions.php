<?php

// include acf fields
require_once('acf-fields.php');


// include custom api ( DON'T REMOVE THIS! )
require_once('api.php');


// enable excerpts for pages ( used for meta descriptions )
add_post_type_support( 'page', 'excerpt' );

// change excerpt text in admin
add_filter( 'gettext', 'change_excerpt', 10, 2 );
function change_excerpt( $translation, $original ) {
    if ( 'Excerpt' == $original ) {
        return 'Page Description';
    } else {
        $pos = strpos($original, 'Excerpts are optional hand-crafted summaries of your');
     
        if ($pos !== false) {
            return  'Page Descriptions are used for SEO and social sharing.';
        }
    }

    return $translation;
}


// removes unnecessary WP emoji styles
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_head', 'wp_generator');
remove_action('wp_print_styles', 'print_emoji_styles');


// hide posts and comments from the admin menu
add_action('admin_menu','remove_menu_items');
function remove_menu_items() {
    remove_menu_page('edit.php');
    remove_menu_page( 'edit-comments.php' );
}


// add acf values to revisions rest api ( DON'T REMOVE THIS! )
add_filter( 'rest_prepare_revision', function( $response, $post ) {
    $data = $response->get_data();
    $data['acf'] = get_fields( $post->ID );

    return rest_ensure_response( $data );
}, 10, 2 );

// handle preview changes ( DON'T REMOVE THIS! )
function custom_preview_page_link($link) {
    $nonce = wp_create_nonce( 'wp_rest' );

    $id = get_the_ID();
    $post = get_post($id);

    $post_type = $post->post_type;
    $post_template = get_page_template_slug($id);

    $link = '/preview/?id=' . $id . '&nonce=' . $nonce . '&type=' . $post_type;

    if ($post_template) {
        $post_template = str_replace('.php', '', $post_template);
        $link = '/preview/?id=' . $id . '&nonce=' . $nonce . '&type=' . $post_type . '&template=' . $post_template;
    }
    
    return $link;
}
add_filter('preview_post_link', 'custom_preview_page_link');
