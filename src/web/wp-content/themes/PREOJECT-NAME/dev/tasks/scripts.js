var gulp = require('gulp');

var browserify = require('browserify');
var jshint = require('gulp-jshint');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
var preprocess = require('gulp-preprocess');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var vueify = require('vueify');
var babelify = require('babelify').configure({
    presets: ['es2015'],
    plugins: ['transform-html-import-to-string'],
});

// settings & configuration
var jsHintSettings = require('../settings/jshint');
var paths = require('../settings/paths');
var onError = require('../settings/plumber.js').onError;

// envs
var environments = require('gulp-environments');
var development = environments.development;
var production = environments.production;


gulp.task('browserify', function() {
    var b = browserify({
        entries: paths.assets + 'js/app.js',
    });
    b.transform(babelify);
    b.transform(vueify);
    // b.transform(handlebars, { traverse: true, });
    
    return b.bundle()
        .on('error', onError)
        .pipe(plumber({
            errorHandler: onError,
        }))
        
        .pipe(source('site.min.js'))
        
        .pipe(buffer())
        
        // preprocess to add environment variables
        .pipe(preprocess({
            context: {
                ENVIRONMENT: process.env.NODE_ENV,
            }
        }))
        
        // only minify for production
        .pipe(production(uglify()))
        
        // save to static
        .pipe(gulp.dest(paths.static + 'js'));

});


gulp.task('hint', function() {
    return gulp.src([
            paths.assets + 'js/*.js',
            paths.assets + 'js/**/*.js',
            "!" + paths.assets + 'js/lib/**', // don't hint lib files
        ])
        .pipe(jshint(jsHintSettings))
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'));
});