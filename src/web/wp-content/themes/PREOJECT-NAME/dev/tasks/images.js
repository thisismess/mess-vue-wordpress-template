var gulp = require('gulp');
var changed = require('gulp-changed');
var imageop = require('gulp-image-optimization');
var plumber = require('gulp-plumber');
var paths = require('../settings/paths.js');
var onError = require('../settings/plumber.js').onError;

gulp.task('optimizeAll', function() {
    return gulp.src([
            paths.assets + 'img/**/*.{png,svg}',
        ])
        .pipe(plumber({
            errorHandler: onError,
        }))
        .pipe(imageop({
            optimizationLevel: 5,
        }))
        .pipe(gulp.dest(paths.static + 'img'))
});


gulp.task('images', function() {
    return gulp.src([
            paths.assets + 'img/**/*.{png,jpg,jpeg,gif,svg}',
        ])
        .pipe(plumber({
            errorHandler: onError,
        }))
        .pipe(changed(paths.static + 'img'))
        .pipe(imageop({
            optimizationLevel: 5,
        }))
        // save to static
        .pipe(gulp.dest(paths.static + 'img'));
});