var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var paths = require('../settings/paths.js');
var onError = require('../settings/plumber.js').onError;


gulp.task('scss', function() {
    return gulp.src([
            paths.assets + 'scss/**/*.scss',
            '!' + paths.assets + 'scss/lib/**/_*.scss', // don't process files that begin with _
            '!' + paths.assets + 'scss/lib/**/*.scss', // don't process lib files
        ])
        .pipe(plumber({
            errorHandler: onError,
        }))
    
        // transpile and minify
        .pipe(sass({outputStyle: 'compressed'}))
        
        // add vendor prefixes
        .pipe(autoprefixer({
            browsers: [
                'last 3 versions',
                'ie >= 9',
            ],
            cascade: false,
        }))
        
        // save to static
        .pipe(gulp.dest(paths.static + 'css'))
        
        // inject styles
        .pipe(browserSync.stream());
});