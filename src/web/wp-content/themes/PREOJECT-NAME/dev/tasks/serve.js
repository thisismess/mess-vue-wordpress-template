var gulp = require('gulp');
var watch = require('gulp-watch');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var runSequence = require('run-sequence');
var paths = require('../settings/paths.js');

// watch / reload tasks
gulp.task('serve', ['build'], function() {
    browserSync({
        proxy: '127.0.0.1:8000',
        open: false,
    });

    // watch scss
    watch(paths.assets + 'scss/**/*.scss', function() {
        gulp.start(['scss']);
    });
    
    // watch scripts and templates
    watch([
            paths.assets + 'js/**/!(*.min).js',
            paths.assets + 'js/**/!(*.min).hbs',
            paths.assets + 'js/**/!(*.min).html',
        ],
        function() {
            runSequence(['hint', 'browserify'], reload);
        }
    );
    
    // watch images
    watch(
        paths.assets + 'img/**/*.{png,jpg,jpeg,gif,svg}',
        ['images']
    );
    
    // watch copy files
    watch(
        paths.assets + 'copy/**/*',
        ['copy']
    );

});