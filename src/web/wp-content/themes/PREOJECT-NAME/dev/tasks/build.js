var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('build', ['scss', 'browserify', 'images', 'copy',]);

gulp.task('build-production', function() {
    runSequence('clean', 'build');
});