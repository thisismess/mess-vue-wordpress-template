var gulp = require('gulp');
var clean = require('gulp-clean');
var confirm = require('gulp-confirm');
var paths = require('../settings/paths.js');

gulp.task('clean', function() {
    return gulp.src(paths.static)
        .pipe(confirm({
            question: "The next task will delete all files in `" + paths.static + "` and recompile. Otherwise, `clean` will be skipped and the rest of the tasks will run. OK? (y/N)",
            input: '_key:y',
        }))
        .pipe(clean({force:true}));
});
