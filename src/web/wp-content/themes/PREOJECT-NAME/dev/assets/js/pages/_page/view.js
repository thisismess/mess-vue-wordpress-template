import Vue from 'vue/dist/vue';
import template from './template.html';

export default Vue.component('PAGENAME', {
    template: template,
    methods: {},
});