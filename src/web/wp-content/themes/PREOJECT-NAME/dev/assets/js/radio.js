import Vue from 'vue/dist/vue.common.js';

/*
There's nothing special about this Vue instance. I'm just using it as a
global events / messaging bus.

Read about Vue instance events here:
https://vuejs.org/v2/api/#Instance-Methods-Events

*/

export default new Vue();