import View from './view';

export default {
    path: undefined,
    name: undefined,
    component: View,
    
    meta: {
        title: "PAGETITLE"
    },
};