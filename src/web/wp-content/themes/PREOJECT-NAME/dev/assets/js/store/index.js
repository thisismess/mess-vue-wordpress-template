import { concat, find } from 'lodash';

export default {
    state: {
        pages: [],
    },
    
    mutations: {
        updatePageList(state, page) {
            let pages = concat(state.pages, page);
            state.pages = pages;
        },
    },
    
    getters: {
        getPageByURL(state) {
            return (url) => {
                return find(state.pages, (page) => {
                    return new URL(page.link).pathname === url;
                });
            };
        },
    },
};
