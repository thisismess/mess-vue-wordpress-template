import Vue from 'vue/dist/vue.common.js';
import template from './template.html';

export default Vue.component('page-default', {
    template: template,
    props: ['page'],
});