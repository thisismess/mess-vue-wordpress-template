import Vue from 'vue/dist/vue.common.js';
import { isArray, isObject, isString, extend, zipObject, take } from 'lodash';

// todo: add garbage collection (remove event listener)

const ARRAY_KEYS = ['eventCategory', 'eventAction', 'eventLabel', 'eventValue'];


Vue.directive('track', {
	inserted(el, binding) {
		let ctx = {
			hitType: 'event',
		};

		if ( binding.value ) {
			if ( isArray(binding.value) ) {
				ctx = extend(
					ctx,
					zipObject(
						take(ARRAY_KEYS, binding.value.length),
						binding.value
					)
				);
			} else if ( isObject(binding.value) ) {
				ctx = extend(ctx, binding.value);
			} else if ( isString(binding.value) ) {
				ctx.eventCategory = binding.value;
			}
		}
		
		el.addEventListener('click', () => {
			console.log(ctx);
			window.ga('send', ctx);
		});
	},
});