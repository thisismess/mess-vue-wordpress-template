import Vue from 'vue/dist/vue.common.js';
import template from '../template.html';
import { get } from 'lodash';
import api from '../../api';
import he from 'he';

export default Vue.component('previewPage', {
    template: template,

    data() {
        return {
            page: undefined,
            loading: true,
            error: false,
        };
    },

    computed: {
        id() {
            return get(this.$route, 'query.id');
        },

        nonce() {
            return get(this.$route, 'query.nonce');
        },

        type() {
            let type = get(this.$route, 'query.type');

            if (type === 'page') {
                type = 'pages';
            }

            return type;
        },

        currentTemplate() {
            let template = get(this.$route, 'query.template');

            if (this.error) {
                template = 'page-error404';
            }

            return template;
        },
    },

    created() {
        this.fetchData();
    },

    methods: {
        fetchData() {
            this.loading = true;

            api.get('/wp-json/wp/v2/' + this.type + '/' + this.id + '/revisions/?_wpnonce=' + this.nonce).then((response) => {
                this.page = response.data[0];
                let title = this.page.title.rendered; 

                document.title = `${he.decode(title)} - ${window.siteName}`;
                this.loading = false;
            }).catch(() => {
                this.error = true;
                this.loading = false;
            });
        },
    },
});
