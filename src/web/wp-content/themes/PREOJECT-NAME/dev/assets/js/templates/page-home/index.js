import Vue from 'vue/dist/vue.common.js';
import template from './template.html';

export default Vue.component('page-home', {
    template: template,
    props: ['page'],
});
