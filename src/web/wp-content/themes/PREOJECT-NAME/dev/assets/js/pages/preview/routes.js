import View from './view';

export default {
    path: '/preview/',
    name: 'previewPage',
    component: View,
};
