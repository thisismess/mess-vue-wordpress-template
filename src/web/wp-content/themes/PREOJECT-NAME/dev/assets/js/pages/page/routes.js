import View from './view';

export default {
    path: '*',
    name: 'templatePage',
    component: View,
};