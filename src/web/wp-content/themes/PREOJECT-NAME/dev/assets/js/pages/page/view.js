import Vue from 'vue/dist/vue.common.js';
import template from '../template.html';
import { filter, delay } from 'lodash';
import api from '../../api';
import he from 'he';
import { TweenLite } from 'gsap';


export default Vue.component('page', {
    template: template,
    
    data() {
        return {
            error: false,
            loading: true,
            scrollTo: null,
            page: {},
        };
    },
    
    computed: {
        pathname() {
            return window.location.pathname;
        },

        slug() {
            let urlParts = this.pathname.split( '/' );
            let filteredURL = filter(urlParts, (item) =>{
                let empty = item === '';
                if (!empty) {
                    return item;
                }
            });

            return filteredURL[filteredURL.length - 1];
        },

        pageClass() {
            return 'page-' + this.page.slug;
        },

        type() {
            let type;
            type = 'pages';

            return type;
        },

        currentTemplate() {
            let template;

            if (this.error) {
                template = 'page-error404';
            } else if (this.page) {
                if (this.page.template) {
                    template = this.page.template.split('.')[0];
                } else {
                    template = 'page-default';
                }
            }

            return template;
        },
    },
    
    watch: {
        $route(from, to) {
            if (this.$route.hash) {
                this.scrollTo = this.$route.hash;
            }
            if (from.path !== to.path) {
                this.fetchData();
            }
        },
    },
    
    created() {
        if (this.$route.hash) {
            this.scrollTo = this.$route.hash;
        }
        this.fetchData();
    },
    
    methods: {
        fetchData() {
            this.loading = true;
            this.error = false;

            let page = this.$store.getters.getPageByURL(this.$route.params[0]);

            if (page) {
                this.page = page;
                this.pageFound();
            } else {
                if (this.pathname === '/') {
                    api.get(`/wp-json/wp/v2/pages/?include=${window.homeID}`).then((response) => {
                        let results = response.data;
                        this.page = results[0];
                        this.$store.commit('updatePageList', this.page);

                        this.pageFound();
                    }).catch(() => {
                        this.newError();
                    });
                } else {
                    api.get(`/wp-json/wp/v2/${this.type}/?slug=${this.slug}`).then((response) => {
                        let results = response.data;

                        if (results.length < 1) {
                            this.getPage();
                        } else {
                            let page = this.findMatch(results);

                            if (page.length < 1) {
                                this.getPage();
                            } else {
                                this.page = page[0];
                                this.$store.commit('updatePageList', this.page);

                                this.pageFound();
                            }
                        }
                    }, (error) => {
                        console.log(error);
                        this.newError();
                    });
                }
            }
        },

        getPage() {
            api.get(`/wp-json/wp/v2/pages/?slug=${this.slug}`).then((response) => {
                let results = response.data;

                if (results.length < 1) {
                    this.newError();
                } else {
                    let page = this.findMatch(results);

                    if (page.length < 1) {
                        this.newError();
                    } else {
                        this.page = page[0];
                        this.$store.commit('updatePageList', this.page);

                        this.pageFound();
                    }
                }
            });
        },

        findMatch(results) {
            return filter(results, (result) => {
                let match = this.pathname === result.link.replace(/https?:\/\/[^\/]+/i, "");
                return match;
            });
        },

        pageFound() {
            let title = this.page.title.rendered;
            document.title = `${he.decode(title)} - ${window.siteName}`;

            this.loading = false;

            this.handleScroll();
        },

        handleScroll() {
            delay(() => {
                if (this.scrollTo) {
                    let headerOffset = 67;
                    let id = this.scrollTo.replace('#', '');
                    let scroller = window;
                    let scrollToEl = document.getElementById(id);
                    let scrollToVal = scrollToEl.offsetTop - headerOffset;

                    TweenLite.to(scroller, 0.5, {
                        scrollTo: scrollToVal,
                    });
                }
                this.scrollTo = null;
            }, 500);
        },

        newError() {
            this.error = true;
            this.loading = false;
            this.scrollTo = null;
        },
    },
});