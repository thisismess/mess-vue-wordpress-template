import 'babel-polyfill';
import 'url-api-polyfill';
import './tracking';

import Vue from 'vue/dist/vue.common.js';

// plugins
import Vuex from 'vuex';
import ScrollView from 'vue-scrollview';
import Vue2TouchEvents from 'vue2-touch-events';
import { each, delay } from 'lodash';

import router from './router';
import store from './store';

// directives
import './directives/track';
import './directives/click-outside';

// global components
// import GlobalFooter from './components/global-footer';

// page templates
import pageDefault from './templates/page-default';
import pageError404 from './templates/page-error404';
import pageHome from './templates/page-home';


const GLOBAL_COMPONENTS = {
    // global components

    // page templates
    'page-default': pageDefault,
    'page-error404': pageError404,
    'page-home': pageHome,
};

each(GLOBAL_COMPONENTS, (value, key) => {
    Vue.component(key, value);
});


console.log('Starting App');

/*
    This is the root Vue instance.
*/

// Vue state
Vue.use(Vuex);

// Vue Scroll
Vue.use(ScrollView);

// Vue Touch Events
Vue.use(Vue2TouchEvents);

window.app = new Vue({
    router: router,
    store: new Vuex.Store(store),

    watch: {
        $route: 'routeChanged',
    },

    created() {
        this.routeChanged();
    },

    methods: {
        routeChanged() {
            delay(() => {
                window.scrollTo(0, 0);
            }, 300);

            // analytics
            if (window.ga) {
                window.ga('set', 'page', this.$route.path);
                window.ga('send', 'pageview');
            }
        },
    },

}).$mount('#app');