/*
    Store environment-specific variables in here. You can retrieve
    the current environment in another file like so:
    
    import environment from 'RELATIVE_PATH_TO/environments.js';
    
    console.log(environment.name);
    // should output current environment name
*/

import { defaultsDeep } from 'lodash';

let environments = {
    // environment-specific settings go here
    
    development: {
        name: 'development',
        TRACKING_ID: '',
    },

    staging: {
        name: 'staging',
        TRACKING_ID: '',
    },

    production: {
        name: 'production',
        TRACKING_ID: '',
    }
};

let globalSettings = {
    // global settings go here
};

let currentEnvironmentName = '/* @echo ENVIRONMENT */' || 'development';
let currentEnvironment = environments[currentEnvironmentName];

defaultsDeep(currentEnvironment, globalSettings);

export default currentEnvironment;