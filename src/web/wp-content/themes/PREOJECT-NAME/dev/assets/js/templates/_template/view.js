import Vue from 'vue/dist/vue';
import template from './template.html';

export default Vue.component('pageHome', {
    template: template,
    methods: {},
});