import Vue from 'vue/dist/vue.common.js';
import template from './template.html';

export default Vue.component('error404', {
    template: template,

    created() {
        document.title = `Page Not Found - ${window.siteName}`;
    },
});