import Vue from 'vue/dist/vue.common.js';
import VueRouter from 'vue-router';

import Page from './pages/page/routes';
import Preview from './pages/preview/routes';

Vue.use(VueRouter);

const routes = [
    Page,
    Preview,
];

let router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'active',
    routes: routes,
});

export default router;