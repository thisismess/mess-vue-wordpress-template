/*
    jshint config
    ------------
    Sets options for jshint. http://jshint.com/docs/options/
 */

module.exports = {
    globals: {
        "require": true,
    },
    
    browserify: true,
    browser: true,
    devel: true,
    
    bitwise: true,
    camelcase: false,
    curly: true,
    eqeqeq: true,
    esnext: true,
    freeze: true,
    immed: true,
    latedef: true,
    newcap: true,
    noarg: true,
    regexp: true,
    undef: true,
    unused: true,
}