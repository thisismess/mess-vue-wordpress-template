/*
    Note: paths are relative to the gulpfile.
*/

module.exports = {
    assets : './assets/', // unprocessed assets
    static: '../static/', // static assets
}