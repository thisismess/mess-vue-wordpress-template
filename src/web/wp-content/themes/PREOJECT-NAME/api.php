<?php

/*
    This is where you'll include any custom api endpoints
    for faster or selected data retrieving.
 */

// add necessary variables
add_action('wp_head','ajaxurl');
function ajaxurl() {
    ?>
        <script type="text/javascript">
            window.wp_admin_ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
            window.homeID = '<?php echo get_option('page_on_front'); ?>';
            window.siteName = '<?php echo get_bloginfo('name'); ?>';
        </script>
    <?php
}

