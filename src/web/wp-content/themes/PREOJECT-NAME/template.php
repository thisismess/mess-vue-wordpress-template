<?php 

    $excerpt = '';
    if (has_excerpt()) {
        $excerpt = wp_strip_all_tags(get_the_excerpt());
    }

?><!DOCTYPE html>
<html>
<head>
    <title><?php echo the_title() . ' - ' . get_bloginfo('name') ?></title>
    <meta name="description" content="<?php echo $excerpt; ?>">
    
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" id="viewport" content="width=device-width,initial-scale=1.0,viewport-fit=cover" />

    <link rel="shortcut icon" type="image/x-icon" href="/wp-content/themes/PROJECT-NAME/static/img/desktop-favicon.png" />
    <link rel="apple-touch-icon" href="/wp-content/themes/PROJECT-NAME/static/img/mobile-favicon.png">

    <link rel="stylesheet" href="/wp-content/themes/PROJECT-NAME/static/css/styles.css?=<?php print WP_PATH; ?>" />
    <script src="/wp-content/themes/PROJECT-NAME/static/js/modernizr.js?=<?php print WP_PATH; ?>"></script>

    <?php wp_head(); ?>

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="<?php echo the_title() . ' - ' . get_bloginfo('name') ?>" />
    <meta name="twitter:description" content="<?php echo $excerpt; ?>" />

    <meta property="og:title" content="<?php echo the_title() . ' - ' . get_bloginfo('name') ?>" />
    <meta property="og:description" content="<?php echo $excerpt; ?>" />
    <meta property="og:url" content="<?php echo get_permalink() ?>" />
    <meta property="og:site_name" content="<?php echo get_bloginfo('name') ?>" />
</head>

<body <?php body_class(); ?>>
    <!-- vue app -->
    <div id="app">
        <router-view :key="$route.path"></router-view>
    </div>
    
    <!-- scripts -->
    <script src="/wp-content/themes/PROJECT-NAME/static/js/site.min.js?=<?php print WP_PATH; ?>"></script>
    <?php wp_footer(); ?>
</body>
</html>