import os, re

from time import gmtime, strftime
from contextlib import closing
from zipfile import ZipFile, ZIP_DEFLATED

from fabric.contrib.files import exists, upload_template
from fabric.contrib.project import upload_project
from fabric.context_managers import path, shell_env
from fabric.utils import indent as ind, abort, warn
from fabric.colors import *
from fabric.api import *
from fabric.contrib.project import rsync_project
from contextlib import contextmanager as _contextmanager

from fabtools import require as needs
from fabtools.python import virtualenv

import fabtools

from environments import *
from database import *
from media import *
from server import *
from env import *
from opbeat import *

from .utils import select_env
from .serverdensity import configure_serverdensity
from .logrotate import configure_logrotate
from .nginx import configure_nginx
from .wordpress import wp_fix_uploads


def log(text, indent=0, color=white):
    print(color(ind(text, indent*4)))


def pull():
    if 'db' not in env.keys():
        select_env()
    #execute(pull_database)
    execute(sync_uploads)
    

def push():
    if 'db' not in env.keys():
        select_env()
    #execute(push_database)
    execute(sync_uploads)
    

def ship():
    if 'branch' not in env.keys():
        select_env()
    execute(release)
    execute(prune)
    
    
def mark():
    if 'branch' not in env.keys():
        select_env()
    register_deployment(env.repo_root)


def release():
    """
    Create a new release on the server specified.
    """
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        sudo('pwd') # We cheat, and prompt for the password first
    log("Creating new release on %s..." % env.host)
    check_environment(1)
    release = strftime('%Y%m%d%H%M%S', gmtime())
    env.release = release
    upload_release(release, 0)
    if 'django_app' in env:
        configure_virtualenv(release, 0)
        collect_static(release, 0)
    wp_fix_uploads(release, 0)
    configure(release, 0)


def check_environment(indent=0):
    log("Checking environment on %s..." % env.host, indent)
    check_filesystem(indent)
    # check_postgres(indent+1) # Something about CentOS breaks this. Investigate later. Sucks :(
    if 'django_app' in env:
        check_redis(indent)
        check_virtualenv(indent)
    

def check_filesystem(indent=0):
    log("Checking filesystem...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        cwd, env.cwd = env.cwd, '/' # we have to swap the cwd cause Fabric prepends it to all commands, and it might not exist yet.
        needs.directory(cwd, owner=env.user, use_sudo=True)
        env.cwd, cwd  = cwd, None # unhack
        needs.directory(os.path.join(env.cwd, 'backups'), owner=env.user, use_sudo=False, mode=755)
        needs.directory(os.path.join(env.cwd, 'releases'), owner=env.user, use_sudo=False, mode=755)
        needs.directory(os.path.join(env.cwd, 'logs'), owner=env.user, use_sudo=False, mode=777)
        needs.directory(os.path.join(env.cwd, 'bin'), owner=env.user, use_sudo=False, mode=777)
        needs.directory(os.path.join(env.cwd, 'uploads'), owner=env.user, use_sudo=False, mode=777)
    log("Filesystem is OK", indent)
    
    
def upload_release(release, indent=0):
    log("Creating new release %s..." % release, indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        # archive project to local tmp dir
        local('mkdir /tmp/%s' % release)
        log("Archiving git repo..", indent)
        local('git archive %s %s | tar -x -C /tmp/%s --strip=1 %s' % (env.branch, env.app_path, release, env.app_path))
        with cd(env.remote_release_path):
            run("mkdir %s" % release)
            # copy previous release to new dir for rsync
            if exists("current") and run('ls %s/current' % env.remote_release_path):
                run("cp -R current/* %s" % release)
            # rsync local tmp archive with previous release copy
            log("Syncing archive to server..", indent)
            rsync_project(remote_dir='%s/%s' % (env.remote_release_path, release), local_dir='/tmp/%s/%s/' % (release, env.local_name), delete=True)
            local('rm -rf /tmp/%s' % release)
            log("Archive uploaded", indent)

    
def configure(release=None, indent=0):
    if not release:
        release = current().split('/')[-1]
        env.release = release
    if 'django_app' in env:
        configure_virtualenv(release, indent)
        migrate_database(release, indent)
        configure_uwsgi(release, indent)
        if env.uses_celeryd:
            configure_celery(release, indent)
    configure_nginx(release, indent)
    if 'sd_agent' in env:
        configure_serverdensity(release, indent)
    configure_logrotate(release, indent)
    start(indent)


def current():
    files = get_releases()
    return files[0]
    

def get_releases():
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        files = run('ls -a %s' % env.remote_release_path)
        files = re.split("[\s]+", files)
        files = filter(lambda f: re.match("^[\d]+", f), files)
        files = map(lambda f: "%s/%s" % (env.remote_release_path, f), files)
        files.sort()
    return files
    

def prune(indent=0):
    log("Pruning old releases...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):    
        files = get_releases()
        keeping = 0 - int(env.keep_releases)
        del files[keeping:]
        for f in files:
            sudo("rm -rf %s" % f)    
    log("%s Releases pruned" % len(files), indent)