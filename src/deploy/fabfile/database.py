from fabric.api import *
from time import gmtime, strftime

from server import stop, start
from .utils import select_env


def pull_psql_database():
    """Sync local postgres db to remote server"""
    fname = "%s_%s.sql" % ( env.db, strftime('%Y%m%d%H%M%S', gmtime() ))
    with settings(user=env.user):
        run( "pg_dump --file=/var/tmp/%s --format=p %s" % ( fname, env.db ))
        local( "scp %s@%s:/var/tmp/%s /var/tmp/%s" % ( env.user, env.hosts[0], fname, fname) )
        try:
            local( "dropdb -h localhost %s" % env.db )
        except:
            pass
        local( "createdb -h localhost %s" % env.db )
        local( "psql -d %s -h localhost -f /var/tmp/%s" % ( env.db, fname ))


def push_psql_database():
    """Sync remote server to local postgres db"""
    fname = "%s_%s.sql" % ( env.db, strftime('%Y%m%d%H%M%S', gmtime() ))
    with settings(user=env.user):
        local( "pg_dump --file=/tmp/%s -h localhost --format=p %s" % ( fname, env.db ))
        local( "scp /tmp/%s %s@%s:/tmp/%s" % ( fname, env.user, env.hosts[0], fname) )
        stop()
        try:
            run( "dropdb %s" % env.db )
        except:
            pass
        run( "createdb %s" % env.db )
        run( "psql -d %s -f /tmp/%s" % ( env.db, fname ))
        start()

def push_sql(sql_file, db = None):
    """Import SQL into remote DB"""
    if db is None:
        db = env.db

    fname = sql_file

    with settings(user=env.user):
        local( "scp %s %s@%s:/tmp/%s" % ( fname, env.user, env.hosts[0], fname) )
        stop()
        run( "psql -d %s -f /tmp/%s" % ( db, fname ))
        start()        
