def configure_uwsgi(release, indent=0):
    log("Configuring Uwsgi...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_wsgi_path = os.path.join(env.remote_release_path, env.release, env.project)
        env.remote_python_path = os.path.join(env.remote_release_path, env.release)
        env.remote_wsgi_ini_path = os.path.join(env.remote_python_path, 'uwsgi.ini')
        env.uwsgi_env = generate_uwsgi_env()
        upload_template(env.local_uwsgi_ini, '/tmp/%s.uwsgi.ini' % release, context=env)
        upload_template(env.local_uwsgi_config, '/tmp/%s.uwsgi.conf' % release, context=env)
        sudo('mv /tmp/%s.uwsgi.ini  %s' % (release, env.remote_wsgi_ini_path))
        sudo('mv /tmp/%s.uwsgi.conf /etc/init/%s.conf' % (release, env.project))
        sudo('chown root:root /etc/init/%s.conf' % env.project)
    log("Uwsgi is OK", indent)