import sys

from fabric.api import *
from fabric.context_managers import lcd
from .utils import set_blocking, log


def build_production_ember():
    log("Building production ember app...")
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        with lcd(env.ember_app):
            local('ember build --environment="production"')
        local('git commit --allow-empty -am "Ember build for %s"' % env.release)
        set_blocking(sys.stdout)
        set_blocking(sys.stderr)
    log('Ember OK')