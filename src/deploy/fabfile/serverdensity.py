from fabric.api import *
from fabric.contrib.files import exists, upload_template
from .utils import log


def configure_serverdensity(release, indent=0):
    log("Configuring ServerDensity Agent...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        upload_template(env.local_sd_config, '/tmp/%s.sd-config.cfg' % release, context=env)
        sudo('mv /tmp/%s.sd-config.cfg  /etc/sd-agent/config.cfg' % release)
        sudo('chown sd-agent:sd-agent /etc/sd-agent/config.cfg')
    log("ServerDensity is OK", indent)