import sys
from fabric.api import *
from .utils import set_blocking


def tag_release():
    if 'branch' in env:
        local('git tag -a r%s -m "Release to %s, %s"' % (env.release, env.host, env.release))
        set_blocking(sys.stdout)
        set_blocking(sys.stderr)