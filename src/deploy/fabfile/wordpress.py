# Things WordPress does poorly.

from environments import *
from fabric.api import *
from utils import log
from fabric.contrib.console import confirm


def wp_fix_uploads(release, indent=0):
    log("Fixing WordPress uploads folder...")
    if confirm("Push local uploads to remote?", default=True):
        local('rsync -avh %s/ %s@%s:%s' % (env.local_uploads_path, env.user, env.hosts[0], env.remote_uploads_path))
    with cd(os.path.join(env.remote_release_path, release)):
        sudo('rm -rf %s' % env.wordpress_uploads_path)
        sudo('ln -s %s %s' % (env.remote_uploads_path, env.wordpress_uploads_path))
        log("Uploads fixed")