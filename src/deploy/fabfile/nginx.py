import os

from fabric.api import *
from fabric.contrib.files import exists, upload_template
from fabtools import require as needs
from fabtools.python import virtualenv

from .utils import log


def configure_nginx(release=None, indent=0):
    if not release:
        release = current().split('/')[-1]
        env.release = release
    log("Configuring nginx...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_release_path = os.path.join(env.remote_release_path, env.release)
        if 'django_app' in env:
            env.remote_statics_path = os.path.join(env.remote_release_path, env.release, env.django_app, 'static')
        else:
            env.remote_statics_path = os.path.join(env.remote_release_path, env.release, env.project, 'static')
        needs.nginx.site(env.project, template_source=env.local_nginx_config, **dict(env))
        sudo('chmod 775 %s' % env.remote_statics_path)
    log("Nginx is OK", indent)