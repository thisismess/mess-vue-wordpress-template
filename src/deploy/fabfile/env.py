from fabric.api import *

def generate_uwsgi_env():
    if hasattr(env, 'setenv') and len(env.setenv) > 0:
        items = []
        for key,value in env.setenv.iteritems():
            items.append('env = %s=%s' % (key,value))
        return "\n".join(items)
    return ""