import os

from fabric.contrib.files import exists, upload_template
from fabric.api import *
from fabtools import require as needs
from fabtools.python import virtualenv

from .utils import log
from .env import generate_uwsgi_env, generate_celery_env
from .server import start, stop
from .database import migrate_database
from .serverdensity import configure_serverdensity

def configure(release, indent=0):
    configure_virtualenv(release, indent)
    migrate_database(release, indent)
    if env.uses_supervisord:
        configure_supervisord(release, indent)
    if not env.uses_supervisord:
        configure_uwsgi(release, indent)
    if not env.uses_supervisord and env.uses_celeryd:
        configure_celery(release, indent)
    if 'sd_agent' in env and env.sd_agent:
        configure_serverdensity(release, indent)
    configure_nginx(release, indent)
    start(indent)


def configure_nginx(release, indent=0):
    log("Configuring nginx...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_python_path = os.path.join(env.remote_release_path, env.release)
        if 'django_app' in env:
            env.remote_statics_path = os.path.join(env.remote_release_path, env.release, env.django_app, 'static')
        else:
            env.remote_statics_path = os.path.join(env.remote_release_path, env.release, env.project, 'static')
        needs.nginx.site(env.project, template_source=env.local_nginx_config, **dict(env))
        sudo('chmod 775 %s' % env.remote_statics_path)
    log("Nginx is OK", indent)


def configure_supervisord(release, indent=0):
    log("Configuring Supervisord...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_wsgi_path = os.path.join(env.remote_release_path, env.release, env.project)
        env.remote_python_path = os.path.join(env.remote_release_path, env.release)
        env.remote_project_path = os.path.join(env.remote_release_path, env.release)
        env.remote_wsgi_ini_path = os.path.join(env.remote_python_path, 'uwsgi.ini')
        env.celery_env = generate_celery_env()
        env.uwsgi_env = generate_uwsgi_env()
        upload_template(env.local_uwsgi_ini, '/tmp/%s.uwsgi.ini' % release, context=env)
        upload_template(env.local_supervisord_config, '/tmp/%s.supervisord.conf' % release, context=env)
        sudo('mv /tmp/%s.uwsgi.ini  %s' % (release, env.remote_wsgi_ini_path))
        sudo('mv /tmp/%s.supervisord.conf /etc/supervisor/conf.d/%s.conf' % (release, env.project))
        sudo('chown root:root /etc/supervisor/conf.d/%s.conf' % env.project)
    log("Supervisord is OK", indent)


def configure_uwsgi(release, indent=0):
    log("Configuring Uwsgi...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_wsgi_path = os.path.join(env.remote_release_path, env.release, env.project)
        env.remote_python_path = os.path.join(env.remote_release_path, env.release)
        env.remote_wsgi_ini_path = os.path.join(env.remote_python_path, 'uwsgi.ini')
        env.uwsgi_env = generate_uwsgi_env()
        upload_template(env.local_uwsgi_ini, '/tmp/%s.uwsgi.ini' % release, context=env)
        upload_template(env.local_uwsgi_config, '/tmp/%s.uwsgi.conf' % release, context=env)
        sudo('mv /tmp/%s.uwsgi.ini  %s' % (release, env.remote_wsgi_ini_path))
        sudo('mv /tmp/%s.uwsgi.conf /etc/init/%s.conf' % (release, env.project))
        sudo('chown root:root /etc/init/%s.conf' % env.project)
    log("Uwsgi is OK", indent)


def configure_celery(release, indent=0):
    log("Configuring Celery...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_python_path = os.path.join(env.remote_release_path, env.release)
        env.remote_project_path = os.path.join(env.remote_release_path, env.release)
        upload_template(env.local_celery_config, '/tmp/%s' % release, context=env)
        sudo('mv /tmp/%s /etc/init/%s-celery.conf' % (release, env.project))
        sudo('chown root:root /etc/init/%s-celery.conf' % env.project)
    log("Celery is OK", indent)


def configure_virtualenv(release, indent=0):
    log("Installing python packages...", indent)
    print env.remote_virtualenv
    with virtualenv(env.remote_virtualenv):
        env.remote_requirements_file = os.path.join(env.remote_release_path, env.release, 'requirements.txt')
        needs.python.requirements(env.remote_requirements_file)
    log("Python packages OK...", indent)