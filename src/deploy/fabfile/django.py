def collect_static(release='../public', indent=0):
    log("Collecting static files...", indent)
    with virtualenv(env.remote_virtualenv):
        with cd(env.remote_release_path):
            with shell_env(**env.setenv):
                run( "rm -rf %s/static/*" % release)
                run( 'python %s/manage.py collectstatic --noinput' % release)
    log("Static files are OK", indent)
    
    
def migrate_database(release, indent=0):
    log("Migrating database...", indent)
    with settings():
        with virtualenv(env.remote_virtualenv):
            with cd(env.remote_release_path):
                with shell_env(**env.setenv):
                    run( 'python %s/manage.py syncdb' % release)
                    run( 'python %s/manage.py migrate' % release)
    log("Database migration OK", indent)