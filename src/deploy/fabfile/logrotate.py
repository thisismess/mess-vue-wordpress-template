from fabric.api import *
from fabric.contrib.files import exists, upload_template
from .utils import log


def configure_logrotate(release, indent=0):
    log("Configuring Logrotate...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        upload_template(env.local_logrotate_config, '/tmp/%s.lr-config.cfg' % release, context=env)
        sudo('mv /tmp/%s.sd-config.cfg  /etc/logrotate.d/%s' % (release, env.project))
        sudo('chown root:root /etc/logrotate.d/%s' % env.project)
    log("Logrotate is OK", indent)