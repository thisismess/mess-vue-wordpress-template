from fabric.colors import *
from fabric.utils import indent as ind, abort, warn
from fabric.api import *
from fabric.state import env
from fabric.operations import prompt

import environments


def select_env():
    envs = get_envs()
    if envs:
        environment = prompt('Please specify target environment %s: ' % str(envs))
    else:
        environment = prompt('Please specify a target environment: ')
    if not environment in environments.__dict__:
        print('Sorry, but "%s" is not valid environment.\n' % environment)
        select_env()
    else:
        call_envs = getattr(environments, environment)
        execute(environment)
    
def get_envs():
    if 'CURRENT_ENVS' in environments.__dict__:
        return environments.CURRENT_ENVS
    return False


def log(text, indent=0, color=white):
    print(color(ind(text, indent*4)))