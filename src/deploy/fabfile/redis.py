def check_redis(indent):
    log("Checking Redis...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        needs.service.started('redis')
    log("Redis is OK", indent)