def check_postgres(indent=0):
    """
    Checks for the presence of the database.
    """
    log("Checking Postgres...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        needs.postgres.server()
        needs.postgres.database(env.db, env.db_user)
    log("Postgres is OK", indent)