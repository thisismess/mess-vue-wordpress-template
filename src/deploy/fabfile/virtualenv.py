def check_virtualenv(indent=0):
    if 'django_app' in env:
        log("Checking Virtualenv...", indent)
        with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):    
            needs.python.virtualenv(env.remote_virtualenv)
            with virtualenv(env.remote_virtualenv):
                needs.python.package('Django')
                needs.python.package('uwsgi')
        log("Virtualenv is OK", indent)
        
        
def configure_virtualenv(release, indent=0):
    log("Installing python packages...", indent)
    print env.remote_virtualenv
    with virtualenv(env.remote_virtualenv):    
        env.remote_requirements_file = os.path.join(env.remote_release_path, env.release, 'requirements.txt')
        needs.python.requirements(env.remote_requirements_file)
    log("Python packages OK...", indent)