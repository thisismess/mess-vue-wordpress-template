import os
from fabric.api import *
from fabric.contrib.project import rsync_project
from fabric.contrib.project import upload_project
from fabric.contrib.files import exists, upload_template
from fabtools import require as needs
from .utils import log


def upload_rtmp_release(release, indent=0):
    log("Uploading RTMP source...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        # archive project to local tmp dir
        needs.directory(os.path.join(env.cwd, 'bin', 'rtmp'), owner=env.user, use_sudo=False, mode=777)
        env.remote_rtmp_dir = os.path.join(env.remote_bin_path, 'rtmp')
        env.local_rtmp_dir = os.path.join(env.repo_root, 'src', 'rtmp')
        rsync_project(remote_dir=env.remote_bin_path, local_dir=env.local_rtmp_dir, delete=True, exclude=['*.pyc', 'testClient'])


def configure_rtmp(release, indent=0):
    log("Configuring RTMP Server...", indent)
    needs.directory(os.path.join(env.cwd, 'bin', 'rtmp'), owner=env.user, use_sudo=False, mode=777)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_rtmp_dir = os.path.join(env.remote_bin_path, 'rtmp')
        upload_template(env.local_rtmp_config, '/tmp/%s' % release, context=env)
        sudo('mv /tmp/%s /etc/init/%s-rtmp.conf' % (release, env.project))
        sudo('chown root:root /etc/init/%s-rtmp.conf' % env.project)
    log("RTMP is OK", indent)