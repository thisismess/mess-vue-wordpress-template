from environments import *
from fabric.api import *
from utils import log


def start(indent=0):
    log("Starting services...", indent)
    with settings(warn_only=True):
        if 'django_app' in env:
            sudo('initctl reload-configuration')
            if env.uses_celeryd:
                sudo('stop %s-celery' % env.project)
            sudo('stop %s' % env.project)
        sudo('service nginx stop')
        if 'django_app' in env:
            sudo('start %s' % env.project)
            if env.uses_celeryd:
                sudo('start %s-celery' % env.project)
        sudo('service nginx start')
        sudo('service sd-agent start')
    log("Services started", indent)


def stop(indent=0):
    log("Stopping services...", indent)
    with settings(warn_only=True):
        if 'django_app' in env:
            sudo('initctl reload-configuration')
            if env.uses_celeryd:
                sudo('stop %s-celery' % env.project)
                sudo('stop %s' % env.project)
        sudo('service nginx stop')
        sudo('service sd-agent stop')
    log("Services stopped", indent)