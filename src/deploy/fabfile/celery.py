def configure_celery(release, indent=0):
    log("Configuring Celery...", indent)
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        env.remote_python_path = os.path.join(env.remote_release_path, env.release)
        env.remote_project_path = os.path.join(env.remote_release_path, env.release)
        upload_template(env.local_celery_config, '/tmp/%s' % release, context=env)
        sudo('mv /tmp/%s /etc/init/%s-celery.conf' % (release, env.project))
        sudo('chown root:root /etc/init/%s-celery.conf' % env.project)
    log("Celery is OK", indent)