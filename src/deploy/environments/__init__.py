from fabric.api import *
import os


# NEVER STORE PASSWORDS IN THIS FILE. SERIOUSLY. I WILL HUNT YOU DOWN.
# Except for MySQL because ...sigh... it doesn't support local auth.
# Because MySQL is literally the worst database. Goddamn you, MySQL.

CURRENT_ENVS = ('staging', )  # UPDATE THIS WITH CURRENT ENVS


def live():
    env.project = ''
    env.hosts = ['', ]
    env.app_path = 'src/web/'
    env.use_ssh_config = True
    env.branch = 'master'
    env.site = ''
    env.keep_releases = 5
    common()


def staging():
    env.project = ''
    env.hosts = ['', ]
    env.app_path = 'src/web/'
    env.use_ssh_config = True
    env.branch = 'master'
    env.site = ''
    env.keep_releases = 5
    common()



def common():
    # ############################################
    # YOU SHOULD NOT HAVE TO CHANGE ANYTHING BELOW
    # Database Settings
    env.db_host = '127.0.0.1'
    env.db_user = 'mess'
    env.db_password = ''
    env.db = '%s' % env.project
    # Server Settings
    env.user = 'mess'
    env.cwd = "/home/%s/sites/%s" % (env.user, env.project)
    env.remote_name = 'public'
    env.remote_path = os.path.join(env.cwd, env.remote_name)
    env.remote_virtualenv = os.path.join(env.cwd, 'env')
    env.remote_statics_path = os.path.join(env.remote_path, env.project)
    env.remote_uploads_path = os.path.join(env.cwd, 'uploads')
    env.remote_backups_path = os.path.join(env.cwd, 'backups')
    env.remote_logs_path = os.path.join(env.cwd, 'logs')
    # Scale Configs
    env.uswgi_processes = 4
    # Local settings
    env.local_name = 'web'
    env.repo_root = os.path.join(os.path.dirname(__file__), '../../../')
    env.local_path = os.path.join(env.repo_root, env.app_path)
    env.wordpress_uploads_path = os.path.join('wp-content', 'uploads')
    env.local_uploads_path = os.path.join(env.local_path, env.wordpress_uploads_path)
    env.local_plugins_path = os.path.join(env.local_path, 'wp-content', 'plugins')
    env.local_release_path = os.path.join(env.local_path, env.local_name)
    env.local_logrotate_config = os.path.join(env.repo_root, 'etc', 'logrotate.conf')
    env.local_sd_config = os.path.join(env.repo_root, 'etc', 'sd-agent.cfg')
    env.local_nginx_config = os.path.join(env.repo_root, 'etc', 'nginx.conf')
    env.local_uwsgi_config = os.path.join(env.repo_root, 'etc', 'uwsgi.conf')
    env.local_virtualenv = os.path.join(env.repo_root, '.venv/bin/activate')
    # Releases
    env.remote_release_path = "%s/releases" % env.cwd