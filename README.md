# NEW WP/VUE PROJECT

## Initialization

- Search for and replace all instances of `PROJECT-NAME` with your project name.
- Update `/src/web/wp-content/themes/PROJECT-NAME/screenshot.jpg` to an image that makes sense for the theme you're creating. When in doubt, use the site's logo or an image of the homepage from your mocks.
- Include a `desktop-favicon.png` and `mobile-favicon.png` in `/wp-content/themes/PROJECT-NAME/dev/assets/img/`. The mobile favicon should have extra padding on all sides and should not have a transparent background.
- Since Node modules aren't stored in the repository, you'll need to run `make` to install them.
- In `/src/web/wp-config.php`, change the `$table_prefix` to whatever you want for your database table prefix (NOTE: keep it to just a few letters).
- Navigate to <http://localhost:3000> and install Wordpress
- Since you can't send emails on localhost, watch for this postdrop error in your terminal (`postdrop: warning: mail_queue_enter: create file maildrop/436485.5379: No such file or directory`). Once it shows, you'll want to stop and start `make run`. It is then safe to navigate to <http://localhost:8000/wp-admin/>.
- In the admin, go to "Appearance" > "Themes", and activate your custom theme.
- In the admin, go to "Plugins", and activate all necessary plugins ("WP REST API" is required, and "WP Rest API Cache" is very highly recommended).
- In the admin, go to "Settings" > "Permalinks", and change the "Common Settings" to "Custom Structure" and put `/%year%/%monthnum%/%postname%/` as the value.
- In the admin, select a page from "Pages", click on "Screen Options" in the top right corner of the screen and select to show the "Page Description" so you can add page descriptions for SEO and social sharing purposes.



## Running the project

From the base directory, the command `make run` will spin up a BrowserSync server and initialize the watch tasks for processing SCSS and Javascript, live reloading / style injection, and Javascript hinting.

- By default, it will serve at <http://localhost:3000>.
- More information and settings are available at <http://localhost:3001>
- Hinting results will output to the command line. If there are any errors or warnings, fix them.



## Structure

### Development assets and packages

Un-minified, un-concatenated javascript lives in `src/web/wp-content/themes/PROJECT-NAME/dev/assets/js`.

You can use NPM to install packages such as jQuery and Backbone: in `/wp-content/themes/PROJECT-NAME/dev/assets` run `npm install PACKAGE_NAME --save-dev` and you will be able to import it in your scripts using the import syntax.

If you aren't familiar with Vue.js, this probably isn't the template for you. If you want to go ahead and use it anyway, reference the Vue.js V2 documentation: <https://vuejs.org/v2/guide/>


### Final assets

- Javascript assets will compile to `src/web/wp-content/themes/PROJECT-NAME/static/js/site.min.js`
- CSS assets will compile to `src/web/wp-content/themes/PROJECT-NAME/static/css/styles.css`
- All other static assets live in their respective directories: `src/web/wp-content/themes/PROJECT-NAME/static/img/`, `src/web/wp-content/themes/PROJECT-NAME/static/fonts/`, etc. 
