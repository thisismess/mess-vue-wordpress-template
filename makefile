all: web

virtualenv:
	test -d .venv || virtualenv .venv
	. .venv/bin/activate; pip install fabric==1.8.1 fabtools
	touch .venv/bin/activate

gulp:
	cd src/web/wp-content/themes/PROJECT-NAME/dev/; npm install --save-dev
	
web: virtualenv gulp

clean:
	rm -rf .venv
	rm -rf src/web/wp-content/themes/PROJECT-NAME/dev/node_modules
	
mark:
	. .venv/bin/activate; fab -f src/deploy/fabfile mark

pull:
	. .venv/bin/activate; fab -f src/deploy/fabfile pull

run-gulp:
	cd src/web/wp-content/themes/PROJECT-NAME/dev/; NODE_ENV=development gulp serve

run-php:
	cd src/web/; php -S 0.0.0.0:8000 -t .

run:
	${MAKE} -j run-gulp run-php

build:
	cd src/web/wp-content/themes/PROJECT-NAME/dev; echo 'minifying javascript...'; NODE_ENV=production gulp browserify

ship: build
	. .venv/bin/activate; fab -f src/deploy/fabfile ship
